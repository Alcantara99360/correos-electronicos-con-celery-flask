de importación de matraz , render_template, solicitud
desde flask_mail importar correo, mensaje
de apio importar apio Apio

importar redis
PalabraClave = "palabra"
DefinicionClave = "definicion"

r = redis. Redis(host='127.0.0.1', puerto=6379)
r. conjunto("id", -1)


def VerificarPalabraExistente(palabra):
    CantPalabras = r. llen(PalabraClave)
    PalabraExistente = Falso
    para i en rango(CantPalabras):
        PalabraActual = r. lindex(PalabraClave, i). decodificar('utf-8')
        if(PalabraActual == palabra):
            PalabraExistente = Verdadero
            quebrar
    volver PalabraExistente


def AgregarPalabraDef(palabra, definicion):
    r. incr("id")
    r. rpush(PalabraClave, palabra)
    r. rpush(DefinicionClave, definicion)
    print("\n palabra agregada correctamente!")


def ActualizarPalabra(AntiguaPalabra, NuevaPalabra, NuevaDefinicion):
    CantPalabras = r. llen(PalabraClave)
    para i en rango(CantPalabras):
        PalabraActual = r. lindex(PalabraClave, i). decodificar('utf-8')
        if(PalabraActual == AntiguaPalabra):
            r. lset(PalabraClave, i, NuevaPalabra)
            r. lset(DefinicionClave, i, NuevaDefinicion)
            quebrar

    imprimir("\n ! La palabra" + AntiguaPalabra + "fue actualizada!")


def EliminarPalabra(palabra):
    CantPalabras = r. llen(PalabraClave)
    para i en rango(CantPalabras):
        PalabraActual = r. lindex(PalabraClave, i). decodificar('utf-8')
        DefinicionActual = r. lindex(DefinicionClave, i). decodificar('utf-8')
        if(PalabraActual == palabra):
            r. lrem(PalabraClave, i, PalabraActual)
            r. lrem(DefinicionClave, i, DefinicionActual)
            quebrar
    print("\n ¡Palabra eliminada!")


def ShowAllWords():
    CantPalabras = r. llen(PalabraClave)
    palabras = []

    para i en rango(CantPalabras):
        palabras. anexar({"nombre": r. lindex(PalabraClave, i). descodificar(
            "utf-8"), "definicion": r. lindex(DefinicionClave, i). decodificar("utf-8")})
    volver palabras


imprimir(r. llaves())

app = Matraz(__name__)


@app. ruta('/Index')
índice def ():
    render_template de retorno ("Índice.html")


@app. ruta('/AgregarPalabra', métodos=['GET', 'POST'])
def AgregarPalabra():
    si se solicita. método == 'POST':
        palabra = request.form["word"]
        definicion = request.form["meaning"]
        if VerificarPalabraExistente(palabra) == False:
            AgregarPalabraDef(palabra, definicion)
            return render_template("AgregarPalabra.html", message="!!Palabra añadida :)")
        else:
            return render_template("AgregarPalabra.html", message="!!La palabra ya existe :(")

    return render_template("AgregarPalabra.html")


@app.route('/EditarPalabra', methods=['GET', 'POST'])
def EditarPalabra():
    if request.method == 'POST':
        AntiguaPalabra = request.form["oldWord"]
        NuevaPalabra = request.form["word"]
        NuevaDefinicion = request.form["meaning"]

        if VerificarPalabraExistente(AntiguaPalabra):
            ActualizarPalabra(AntiguaPalabra, NuevaPalabra, NuevaDefinicion)

            return render_template("EditarPalabra.html", message=False)
        else:

            return render_template("EditarPalabra.html", message=True)

    return render_template("EditarPalabra.html")


@app.route('/EliminarPalabra', methods=['GET', 'POST'])
def eliminarPalabra():
    if request.method == 'POST':
        palabra = request.form["word"]

        if VerificarPalabraExistente(palabra):
            EliminarPalabra(palabra)
            ShowAllWords()
            return render_template("EliminarPalabra.html", message=False)
        else:
            ShowAllWords()
            return render_template("EliminarPalabra.html", message=True)

    return render_template("EliminarPalabra.html")


@app.route('/ListadoPalabras', methods=['GET', 'POST'])
def listadoPalabra():
    allPalabras = ShowAllWords()

    return render_template("ListadoPalabras.html", palabras=allPalabras)


@app.route('/BuscarSignificado', methods=['GET', 'POST'])
def BuscarSignificado():
    if request.method == 'POST':
        palabra = request.form["palabra"]
        if VerificarPalabraExistente(palabra):
            CantPalabras = r.llen(PalabraClave)
            for i in range(CantPalabras):
                PalabraActual = r.lindex(PalabraClave, i).decode('utf-8')
                if(PalabraActual == palabra):
                    getPalabra = {"palabra": palabra, "definicion": r.lindex(
                        DefinicionClave, i).decode("utf-8")}

                    return render_template("BuscarSignificado.html", ShowWord=getPalabra)
        else:
            return render_template("BuscarSignificado.html", message=True)
    return render_template("BuscarSignificado.html")


app = Matraz(__name__)
aplicación. config['MAIL_SERVER'] = 'smtp.mailtrap.io'
aplicación. config['MAIL_PORT'] = 2525
aplicación. config['MAIL_USERNAME'] = '57f2ff6b9a0188'
aplicación. config['MAIL_PASSWORD'] = 'f82835a15c4057'
aplicación. config['MAIL_USE_TLS'] = Verdadero
aplicación. config['MAIL_USE_SSL'] = Falso

apio = Apio(app. nombre, broker='redis://localhost:6379/0')
mail = Correo(aplicación)


@Apio. tarea
def send_async_email(email_data):
    imprimir(email_data + "hola")
    msg = Mensaje(email_data['asunto'],
                  remitente='aldes@quintero.com',
                  destinatarios=[email_data['a']])
    msg. cuerpo = email_data['cuerpo']
    correo. enviar(msg)

@app. route('/sendEmail', methods=['GET', 'POST'])
def sendEmail():
    si se solicita. método == 'GET':
        render_template de retorno ('sendEmail.html')

    email = solicitud. formulario['email']
    mensaje = solicitud. formulario['mensaje']

    email_data = {
        'asunto': '¡Hola desde el otro lado!',
        'a': correo electrónico,
        'cuerpo': mensaje,
    }

    send_async_email(email_data)


si __name__ == "__main__":
    aplicación. ejecutar(depurar=Verdadero)